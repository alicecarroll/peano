CC=clang++
CFLAGS=`cat ./compile_flags.txt`

all: format test
test:
	${CC} ${CFLAGS} ./test.cpp -o test
	./test
	rm test
format:
	clang-format -i peano/*.hpp
	clang-format -i *.cpp
# vim: noet sw=0
