#ifndef PEANO_BASE_HPP
#define PEANO_BASE_HPP
#include <type_traits>

namespace peano {

    struct zero {};

    template <typename P>
    struct next;

    template <typename N>
    struct prev;

    template <typename L, typename R>
    struct sum;

    template <typename L, typename R>
    struct mul;

    template <typename N>
    using prev_t = typename prev<N>::result;
    template <typename L, typename R>
    using sum_t = typename sum<L, R>::result;
    template <typename L, typename R>
    using mul_t = typename mul<L, R>::result;

    template <typename P>
    struct prev<next<P>> {
        using result = P;
    };

    template <typename L>
    struct sum<L, zero> {
        using result = L;
    };

    template <typename L, typename R>
    struct sum<L, next<R>> {
        using result = next<sum_t<L, R>>;
    };

    template <typename L>
    struct mul<L, zero> {
        using result = zero;
    };

    template <typename L, typename R>
    struct mul<L, next<R>> {
        using result = sum_t<mul_t<L, R>, L>;
    };

}  // namespace peano
#endif
